# CalMonitor

This repository contains software used to monitor the fidelity of the LIGO calibration. 
Design documentation for the LIGO calibration monitoring infrastructure can be found here: https://dcc.ligo.org/LIGO-T2300116.

## Transfer function trends pipeline

The photon calibrator is used as an absolute reference for the LIGO calibration.
We track sinusoidal injections from the photon calibrator across the frequency band of interest as a real-time measure of the calibraiton systematic error at a discrete set of frequencies.
The transfer function of the photon calibrator and the strain channel at the oscillator frequencies is computed through the streaming `gstlal_compute_transfer_functions` pipeline.  
The transfer function trends for each oscillator frequency are broadcasted through `kafka` and can also optionally be written to frame files.
The transfer function trends pipeline also computes a metric stating whether the magnitude and phase of the transfer functions at each oscillator frequency are within a pre-determined magnitude and phase threshold, which is nominally set to 10% in magnitude and 10 degrees in phase.

The pipeline consists of two components:
* a gstlal-based transfer function calculator that publishes results
  to kafka
* a
  [scald](https://git.ligo.org/gstlal-visualisation/ligo-scald)-based
  aggregator that consumes the results from kafka and publishes them
  to InfluxDB

The results are viewable from the InfluxDB Grafana pages:
* [LHO monitoring page](https://gstlal.ligo.caltech.edu/grafana/d/StZk6BPVz/calibration-monitoring?orgId=1&refresh=30s&var-DashDatasource=lho_calibration_monitoring&var-strain_channel=CALCS_STRAIN%22)
* [LLO monitoring page](https://gstlal.ligo.caltech.edu/grafana/d/StZk6BPVz/calibration-monitoring?orgId=1&refresh=30s&var-DashDatasource=llo_calibration_monitoring&var-strain_channel=CALCS_STRAIN%22)


### gstlal_compute_transfer_functions

The `gstlal_compute_transfer_functions`  program calculates the
the transfer function of the IFO response function at certain line
frequencies and publishes the results to kafka.  It uses a config file
included with the source:
```
$ ./bin/gstlal_compute_transfer_functions --config $(CONFIG)
```
* LHO: `config/gstlal_compute_tf_configs_H1.ini`
* LLO: `config/gstlal_compute_tf_configs_L1.ini`


The transfer function trends pipeline can run in the standard `igwn`
conda environment, except it currently requires a custom installation
of the `gstlal` software packages in this environment (to be fixed
with a future IGWN release of `gstlal`), and the latest `pydarm-dev`
installation.  It also needs the location of the `CAL_DATA_ROOT`.
```
CAL_DATA_ROOT=/home/cal/svncommon/aligocalibration/trunk/
```

In the site `cal` environments at the sites this can all be achieved
by running the command with the `cal_env_exec` script, e.g.:
```
$ cal_env_exec ./bin/gstlal_compute_transfer_functions --config $(CONFIG)
```

### calmonitor_scald

The `calmonitor_scald` program uses
[ligo-scald](https://git.ligo.org/gstlal-visualisation/ligo-scald) to
read the transfer function measurements from kafka and publish the
results in InfluxDB.  `scald` uses a YAML based config that defines
the database schema as well as the database "backend" config.  The
sites also have differnt kafka broker URIs:

* LHO: `kafka://cal-test-collect@daq3.dcs:9092`
* LLO: `kafka://cal-test-collect@kafka3.ldas.ligo-la.caltech.edu:9092`

The `calmonitor_scald` script wraps the the `scald` aggregator call
with the appropriate topic/schema commands from the schema config:

```
scald aggregate \
  --config config/scald_config.yml \
  --backend L1 \
  --uri kafka://cal-test-collect@kafka3.ldas.ligo-la.caltech.edu:9092 \
  --data-type timeseries \
  --topic TF_mag \
  --schema TF_mag \
  --topic TF_phase \
  --schema TF_phase \
  --topic coherence \
  --schema coherence \
  --topic lock_state \
  --schema lock_state \
  --topic mag_OK \
  --schema mag_OK \
  --topic phase_OK \
  --schema phase_OK
```

Scald expects the credentials needed to access the InfluxDB to be
passed through the environment via the `INFLUX_USERNAME` and
`INFLUX_PASSWORD` environment variables.  In the site deployments
these variables are defined in the `~cal/.influx_creds` file.  This
file should be readable and writable only by the `cal` user.


## site systemd --user deployments

Both programs are run under `systemd --user` in the site `~cal`
environments with the included systemd service files.  There are two
services:
* `gstlal_compute_transfer_functions.service`
* `calmonitor_scald.service`

The include `install` script installs all needed files into their
appropriate locations in the site `~cal` home directories:
```
$ ./install
```
The following files are installed:
```
/home/cal
├── bin
│   ├── calmonitor_scald
│   └── gstlal_compute_transfer_functions
├── .config/systemd/user
│   ├── gstlal_compute_transfer_functions.service
│   └── calmonitor_scald.service
└── monitoring
    ├── gstlal_compute_tf_configs_L1.ini
    └── scald_config.yml
```
Once installed, run the following command to reload the systemd
config:
```
$ systemctl --user daemon-reload
```

You can then use the `systemctl --user` commands to control and view
the status of the services:
```
(igwn) cal@calib1:~ 0$ systemctl --user status gstlal_compute_transfer_functions.service calmonitor_scald.service
● gstlal_compute_transfer_functions.service - LIGO Calibration response function monitor
   Loaded: loaded (/home/cal/.config/systemd/user/gstlal_compute_transfer_functions.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2023-05-01 09:14:16 PDT; 6h ago
 Main PID: 239154 (python3)
   CGroup: /user.slice/user-4150.slice/user@4150.service/gstlal_compute_transfer_functions.service
           ├─239154 python3 /home/cal/bin/gstlal_compute_transfer_functions --config /home/cal/monitoring/gstlal_compute_tf_configs_H1.ini
           └─239528 luatex --luaonly /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn/lib/python3.9/site-packages/matplotlib/mpl-data/kpsewhich.lua

● calmonitor_scald.service - LIGO Calibration response function monitor, scald InfluxDB populator
   Loaded: loaded (/home/cal/.config/systemd/user/calmonitor_scald.service; enabled; vendor preset: enabled)
  Drop-In: /home/cal/.config/systemd/user/calmonitor_scald.service.d
           └─override.conf
   Active: active (running) since Mon 2023-05-01 09:22:54 PDT; 6h ago
 Main PID: 240793 (scald)
   CGroup: /user.slice/user-4150.slice/user@4150.service/calmonitor_scald.service
           └─240793 /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py39-20230425/bin/python3.9 /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn/bin/scald aggregate --config /home/cal/monitoring/scald_config.yml --backend H1 --uri kafka://cal-test-collect@daq3.dcs:9092 --data-type timeseries --topic TF_mag --schema TF_mag --topic TF_phase --schema TF_phase --topic coherence --schema coherence --topic lock_state --schema lock_state --topic mag_OK --schema mag_OK --topic phase_OK --schema phase_OK
```
